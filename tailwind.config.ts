import type { Config } from "tailwindcss"

const config = {
  darkMode: ["class"],
  content: [
    './pages/**/*.{ts,tsx}',
    './components/**/*.{ts,tsx}',
    './app/**/*.{ts,tsx}',
    './src/**/*.{ts,tsx}',
	],
  prefix: "",
  theme: {
    primary: {
      DEFAULT: "#005669",
      900: "#00718A",
      800: "#0098ba",
      700: "#00bae3",
      600: "#0cd3ff",
      500: "#35daff",
      400: "#5de2ff",
      300: "#85e9ff",
      200: "#aef0ff",
      100: "#d7f8ff",
      50: "#ebfbff",
    },
    secondary: {
      DEFAULT: "#6CB2BC",
      900: "#7bbac3",
      800: "#89c1c9",
      700: "#98c9d0",
      600: "#a7d1d7",
      500: "#b6d9de",
      400: "#c4e0e4",
      300: "#d3e8eb",
      200: "#e2f0f2",
      100: "#f0f7f8",
      50: "#f8fbfc",
    },
    error: {
      1: "#FEF8F6",
      2: "#ff003c",
    },
    accent: {
      1: "#0984E3",
      2: "#FFC252",
    },
    discount: "#19b43c",
    link: "#0984e3",
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
      },
    },
    extend: {
      colors: {
        border: "hsl(var(--border))",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "hsl(var(--foreground))",
        destructive: {
          DEFAULT: "hsl(var(--destructive))",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
        card: {
          DEFAULT: "hsl(var(--card))",
          foreground: "hsl(var(--card-foreground))",
        },
        // Custom colors of design system
        primary: {
          DEFAULT: "#EC2227",
          900: "#00718A",
          800: "#0098ba",
          700: "#00bae3",
          600: "#0cd3ff",
          500: "#35daff",
          400: "#5de2ff",
          300: "#85e9ff",
          200: "#aef0ff",
          100: "#d7f8ff",
          50: "#ebfbff",
        },
        secondary: {
          DEFAULT: "#6CB2BC",
          900: "#7bbac3",
          800: "#89c1c9",
          700: "#98c9d0",
          600: "#a7d1d7",
          500: "#b6d9de",
          400: "#c4e0e4",
          300: "#d3e8eb",
          200: "#e2f0f2",
          100: "#f0f7f8",
          50: "#f8fbfc",
        },
        error: {
          1: "#FEF8F6",
          2: "#ff003c",
        },
        accent: {
          1: "#0984E3",
          2: "#FFC252",
        },
        discount: "#19b43c",
        link: "#0984e3",
        shade: {
          "1-100%": "#FFFFFF",
          "1-85%": "rgba(255, 255, 255, 0.85)",
          "1-75%": "rgba(255, 255, 255, 0.75)",
          "2-100%": "#222222",
          "2-30%": "rgba(34, 34, 34, 0.3)",
          "2-5%": "rgba(34, 34, 34, 0.05)",
        },
        neutral: {
          1: "#FFFFFF",
          2: "#F6F6F6",
          3: "#CECECE",
          4: "#B5B5B5",
          5: "#9D9D9D",
          6: "#848484",
          7: "#6C6C6C",
          8: "#535353",
          9: "#3B3B3B",
          10: "#222222",
        },
      },
      borderRadius: {
        lg: "var(--radius)",
        md: "calc(var(--radius) - 2px)",
        sm: "calc(var(--radius) - 4px)",
      },
      keyframes: {
        "accordion-down": {
          from: { height: "0" },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: "0" },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
      },
    },
  },
  plugins: [require("tailwindcss-animate")],
} satisfies Config

export default config