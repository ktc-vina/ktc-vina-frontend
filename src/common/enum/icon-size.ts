export enum IconSizeProperties {
  large = 'large',
  medium = 'medium',
  small = 'small'
}