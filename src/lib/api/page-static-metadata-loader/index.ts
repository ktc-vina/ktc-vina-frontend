import { LandingPageMetadata } from "@/app/page";
// import { axiosClient } from "../config";
import { ProductTypeMetadata } from "@/app/products/all-products-section";
import { ServiceTypeMetadata } from "@/app/services/(sections)/all-services-section";
import { ProductTypeDetailMetadata } from "@/app/products/[type]/page";

import LandingPageMetadataJson from '@/mock/app-data/landing-page.json';
import ProductTypeMetadataJson from '@/mock/app-data/products/index.json';
import ServiceTypeMetadataJson from '@/mock/app-data/all-services.json';
import ProductTypeDetailMetadataJson from '@/mock/app-data/products/goi-do-vong-bi/index.json'

async function getPageMetadata<T>(urlService: string): Promise<T> {
  try {
    const response = await fetch(urlService);
    console.log(response)
    return response as T;
  } catch (error) {
    console.log(error);
    throw error;
  }
}


export const getLandingPageMetadata = async (): Promise<LandingPageMetadata> => {
  // return await getPageMetadata<LandingPageMetadata>('/app-data/landing-page.json');
  return LandingPageMetadataJson as LandingPageMetadata;
};

export const getAllProductsPageMetadata = async (): Promise<ProductTypeMetadata[]> => {
  // return await getPageMetadata<ProductTypeMetadata[]>('/app-data/products/index.json');
  return ProductTypeMetadataJson as ProductTypeMetadata[];
}

export const getAllServicesPageMetadata = async (): Promise<ServiceTypeMetadata[]> => {
  // return await getPageMetadata<ServiceTypeMetadata[]>('/app-data/all-services.json');
  return ServiceTypeMetadataJson as ServiceTypeMetadata[];
}

export const getProductsByTypeMetadata = async (typeId: string = 'goi-do-vong-bi'): Promise<ProductTypeDetailMetadata> => {
  // return await getPageMetadata<ProductTypeDetailMetadata>(`/app-data/products/${typeId}/index.json`);
  return ProductTypeDetailMetadataJson as ProductTypeDetailMetadata;
}