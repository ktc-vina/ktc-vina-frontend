import { ResponseData } from '../types/reponse/response-data';
import { axiosClient } from './config';

type User = {
  name: string;
};

export const getUsers = async (): Promise<ResponseData<User>> => {
  try {
    const response = await axiosClient.get("/users.json");
    return {
      data: response?.data,
      error: '',
      code: response.status,
    } as ResponseData<User>;
  } catch (error) {
    console.log(error)
    return {
      data: null,
      error: 'Error',
      code: 500,
    } as ResponseData<User>;
  }
};
