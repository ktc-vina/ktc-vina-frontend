export interface ResponseData<T> {
    code: number;
    data: T | null;
    error: string | null;
}