import { ImageMetadata } from "./image-meta";

export interface GenericServiceItem {
  id?: string;
  name: string;
  routerLink: string;
  image: ImageMetadata;
  hoverImage?: ImageMetadata;
}