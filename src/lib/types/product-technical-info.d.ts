export default interface ProductTechnicalInfo {
  type: string;
  brand: string;
  code: string;
  from: string;
}