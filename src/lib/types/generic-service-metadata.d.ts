export interface GenericServiceMetadata {
  title: string;
  shortDescription: string;
  items: GenericServiceItem[]
}