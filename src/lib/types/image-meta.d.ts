export interface ImageMetadata {
  url: string,
  width: number,
  height: number,
  alt?: string
}