export interface BreadcrumbRouteMetadata {
  title: string;
  routerLink: string;
}