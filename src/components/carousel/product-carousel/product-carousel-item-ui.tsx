"use client"

import AppImage from "@/components/image-container";
import { ImageMetadata } from "@/lib/types/image-meta";
import Link from "next/link";
import { useState } from "react";

export default function ProductCarouselItemUi(props: {
  imageContainerWidth?: number,
  imageContainerHeight?: number,
  image: ImageMetadata,
  hoverImage?: ImageMetadata,
  title: string,
  routerLink: string
}) {
  const hoverImage = props.hoverImage ?? props.image;

  const containerWidthValue = props.imageContainerWidth ?? props.image.width;
  const containerHeightValue = props.imageContainerHeight ?? props.image.height;

  const [hover, setHover] = useState(false);

  return (
    <Link
      href={ props.routerLink }
      className="group"
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
    >
      {hover === true ? (
        <AppImage 
          containerWidth={containerWidthValue}
          containerHeight={containerHeightValue}
          image={props.image}
        />
      ) : (
        <AppImage 
          containerWidth={containerWidthValue}
          containerHeight={containerHeightValue}
          image={hoverImage}
        />
      )}
      <h6 className="text-neutral-7 mt-2">
        { props.title }
      </h6>
    </Link>
  )
}