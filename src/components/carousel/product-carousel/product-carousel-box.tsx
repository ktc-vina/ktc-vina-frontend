import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious, TopCarouselNext, TopCarouselPrevious } from "@/components/ui/carousel";
import { Button } from "@/components/ui/button"
import ProductCarouselItemUi from "./product-carousel-item-ui";
import { GenericServiceItem } from "@/lib/types/generic-service-item";
import Link from "next/link";

export default function ProductCarouselBox(
  props: {
    routerLink?: string,
    title: string,
    shortDescription: string,
    items: GenericServiceItem[],
    className?: string
  }
) {
  return (
    // Container
    <div className={`${props.className} mb-[10px] p-4 bg-white rounded-xl`}>
      <h3 className="text-primary">{ props.title }</h3>
      {props.shortDescription ? (<div className="text-neutral-7 subtitle-1 mt-1">{ props.shortDescription }</div>) : (<></>)}
      <Carousel>
          {/* Buttons section */}
          <div className="flex justify-between my-5">
            <Link href={props.routerLink ?? '#'}>
              <Button variant="outline" className="text-neutral-8 text-[18px] hover:text-primary hover:border-primary">
                Xem tất cả
              </Button>
            </Link>
            <div className="flex gap-2">
              <TopCarouselPrevious></TopCarouselPrevious>
              <TopCarouselNext></TopCarouselNext>
            </div>
          </div>
          <CarouselContent className="items-start">
            {props.items.map(item => (
              <CarouselItem className="basis-1/4" key={ item.routerLink }>
                <ProductCarouselItemUi
                  image = { item.image }
                  hoverImage = { item.hoverImage }
                  title = { item.name }
                  routerLink= { item.routerLink }
                ></ProductCarouselItemUi>
            </CarouselItem>
            ))}
          </CarouselContent>
        </Carousel>
    </div>
  );
}
