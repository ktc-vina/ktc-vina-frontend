import { Button } from "@/components/ui/button";
import { Carousel, CarouselContent, CarouselItem, TopCarouselNext, TopCarouselPrevious } from "@/components/ui/carousel";
import { GenericServiceItem } from "@/lib/types/generic-service-item";
import ProductCarouselItemUi from "./product-carousel-item-ui";

export default function ProductCarouselDoubleLineItems(props: {
  items: GenericServiceItem[][]
}) {
  return (
    <div>
      <Carousel>
          {/* Buttons section */}
          <div className="flex justify-between mt-[20px] mb-[24px]">
            <Button variant="outline" className="text-neutral-8 hover:text-primary hover:border-primary">Xem tất cả</Button>
            <div className="flex gap-2">
              <TopCarouselPrevious></TopCarouselPrevious>
              <TopCarouselNext></TopCarouselNext>
            </div>
          </div>
          <CarouselContent className="items-start justify-stretch">
            {props.items.map(item => (
              <div className="min-w-0 shrink-0 grow-0 basis-1/4" key={ item[0].routerLink }>
                {/* TODO: replace with grid display for space evenly */}
                <CarouselItem className="h-[243.16px]">
                  <ProductCarouselItemUi
                    imageContainerWidth={280}
                    imageContainerHeight={182}
                    image = { item[0].image }
                    hoverImage = { item[0].hoverImage }
                    title = { item[0].name }
                    routerLink= { item[0].routerLink }
                  ></ProductCarouselItemUi>
                </CarouselItem>
                <CarouselItem className="mt-3">
                  <ProductCarouselItemUi
                    imageContainerWidth={280}
                    imageContainerHeight={182}
                    image = { item[1].image }
                    hoverImage = { item[1].hoverImage }
                    title = { item[1].name }
                    routerLink= { item[1].routerLink }
                  ></ProductCarouselItemUi>
                </CarouselItem>
              </div>
            ))}
          </CarouselContent>
        </Carousel>
    </div>
  )
}