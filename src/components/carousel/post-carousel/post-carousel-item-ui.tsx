"use client"

import { ImageMetadata } from "@/lib/types/image-meta";
import Image from "next/image";
import Link from "next/link";
import { useState } from "react";

export default function PostCarouselItemUi(props: {
  image: ImageMetadata,
  hoverImage?: ImageMetadata,
  title: string,
  routerLink: string
}) {
  const hoverImage = props.hoverImage ?? props.image;
  const [hover, setHover] = useState(false);

  return (
    <Link
      href={ props.routerLink }
      className="group hover:bg-blend-darken"
      onMouseOver={() => setHover(true)}
      onMouseOut={() => setHover(false)}
    >
      {/* TODO: other approach Hover image */}
      <div className="relative">
        {hover === true ? (
          <Image
            src={ hoverImage.url }
            alt="service dummy"
            width={ hoverImage.width }
            height={ hoverImage.height }
            className="w-full h-auto"
          >
          </Image>
        ) : (
          <Image
            src={ props.image.url }
            alt="service dummy"
            width={ props.image.width }
            height={ props.image.height }
            className="w-full h-auto"
          >
          </Image>
        )}
        <div className="absolute transition duration-0 bg-black/0 group-hover:bg-black/30 w-full h-full top-0 rounded-[24px]"></div>
        <h6 className="text-white px-4 py-4 w-full bottom-0 absolute">
          { props.title }
        </h6>
      </div>
    </Link>
  )
}