import { Carousel, CarouselContent, CarouselItem, TopCarouselNext, TopCarouselPrevious } from "@/components/ui/carousel";
import { Button } from "@/components/ui/button"
import { GenericServiceItem } from "@/lib/types/generic-service-item";
import PostCarouselItemUi from "./post-carousel-item-ui";

export default function PostCarouselBox(
  props: {
    title: string,
    shortDescription: string,
    items: GenericServiceItem[],
    className?: string
  }
) {
  return (
    // Container
    <div className={`${props.className} mb-2.5 p-4 rounded-xl`}>
      <h3 className="text-primary">{ props.title }</h3>
      {props.shortDescription && (<div className="text-neutral-7 text-[18px] mt-1">{ props.shortDescription }</div>)}
      <Carousel>
          {/* Buttons section */}
          <div className="flex justify-between mt-3 mb-4">
            <Button variant="outline" className="text-neutral-8 hover:text-primary hover:border-primary text-[18px]">Xem tất cả</Button>
            <div className="flex gap-2">
              <TopCarouselPrevious></TopCarouselPrevious>
              <TopCarouselNext></TopCarouselNext>
            </div>
          </div>
          <CarouselContent className="items-start">
            {props.items.map(item => (
              <CarouselItem className="basis-1/3" key={ item.routerLink }>
                <PostCarouselItemUi
                  image = { item.image }
                  hoverImage = { item.hoverImage }
                  title = { item.name }
                  routerLink= { item.routerLink }
                ></PostCarouselItemUi>
            </CarouselItem>
            ))}
          </CarouselContent>
        </Carousel>
    </div>
  );
}
