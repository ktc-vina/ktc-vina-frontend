import * as React from "react"

import {
  Carousel,
  CarouselContent,
  CarouselItem,
  CarouselNext,
  CarouselPrevious,
} from "@/components/ui/carousel"
import { ImageMetadata } from "@/lib/types/image-meta"
import Image from "next/image"

export function ImageCarousel(props: {
  images: ImageMetadata[]
}) {
  return (
    <Carousel className="w-full max-w-xs">
      <CarouselContent>
        {props.images.map((image, index) => (
          <CarouselItem key={index}>
            <div className="p-1">
              <Image src={image.url} width={image.width} height={image.height} alt="image-index"/>
            </div>
          </CarouselItem>
        ))}
      </CarouselContent>
      <CarouselPrevious />
      <CarouselNext />
    </Carousel>
  )
}
