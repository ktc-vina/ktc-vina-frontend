import * as React from "react"

import { cn } from "@/common/utils/utils"

export interface InputProps
  extends React.InputHTMLAttributes<HTMLInputElement> {}

const Input = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, type, ...props }, ref) => {
    return (
      <input
        type={type}
        className={cn(
          "flex h-10 w-full rounded-md border border-input bg-background px-3 py-2 text-sm ring-offset-background file:border-0 file:bg-transparent file:text-sm file:font-medium placeholder:text-muted-foreground focus-visible:outline-none focus-visible:ring-2 focus-visible:ring-ring focus-visible:ring-offset-2 disabled:cursor-not-allowed disabled:opacity-50",
          className
        )}
        ref={ref}
        {...props}
      />
    )
  }
)
Input.displayName = "Input"

const EmailSubscribeInput = React.forwardRef<HTMLInputElement, InputProps>(
  ({ className, type, ...props }, ref) => {
    return (
      <input
        type={type}
        className={cn(
          "flex w-full focus:outline-none rounded-xl border border-input text-neutral-9 bg-background/80 px-3 py-3 text-[16px] font-medium placeholder:text-muted-foreground placeholder:text-neutral-5",
          className
        )}
        ref={ref}
        {...props}
      />
    )
  }
)
EmailSubscribeInput.displayName = "EmailSubscribeInput"

export { Input, EmailSubscribeInput }
