import Image from "next/image"

export default function ImageContainer(
  props: {
    image: string,
    alt: string,
    width?: number,
    height?: number,
    borderRadius: string,
  }
) {
  return <div className="relative">
    <Image
      src = { props.image }
      alt = { props.alt }
      width = { props.width ?? 0 }
      height = { props.height ?? 0 }
      fill
      style= {{
        objectFit: "cover",
        borderRadius: props.borderRadius, //👈 and here you can select border radius
      }}
    />
  </div>
}