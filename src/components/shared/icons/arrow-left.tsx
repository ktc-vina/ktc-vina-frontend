import React from 'react';
import { IconSizeProperties } from '@/common/enum/icon-size';

export default function ArrowLeft(props: { size: IconSizeProperties }) {
  switch (props.size) {
    case IconSizeProperties.large:
    case IconSizeProperties.medium:
      return (
        <svg
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M17 4L8 12L17 20'
            stroke='#636E72'
            strokeWidth='1.8'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
    case IconSizeProperties.small:
      return (
        <svg
          width='16'
          height='16'
          viewBox='0 0 16 16'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M10 3.5L5 8L10 12.5'
            stroke='#636E72'
            strokeWidth='1.4'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
  }
}
