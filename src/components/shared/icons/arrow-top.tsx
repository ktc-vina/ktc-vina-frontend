import React from 'react';
import { IconSizeProperties } from '@/common/enum/icon-size';

export default function ArrowTop(props: { size: IconSizeProperties }) {
  switch (props.size) {
    case IconSizeProperties.large:
    case IconSizeProperties.medium:
      return (
        <svg
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M20 16.5L12 7.5L4 16.5'
            stroke='#636E72'
            strokeWidth='1.8'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
    case IconSizeProperties.small:
      return (
        <svg
          width='16'
          height='16'
          viewBox='0 0 16 16'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M12.5 12.5L8 7.5L3.5 12.5'
            stroke='#636E72'
            strokeWidth='1.4'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
  }
}
