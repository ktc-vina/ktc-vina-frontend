import React from 'react';
import { IconSizeProperties } from '@/common/enum/icon-size';

export default function Close(props: { size: IconSizeProperties }) {
  switch (props.size) {
    case IconSizeProperties.large:
    case IconSizeProperties.medium:
      return (
        <svg
          width='24'
          height='24'
          viewBox='0 0 24 24'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M19 19L5 5'
            stroke='#2D3436'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
          <path
            d='M19 4.99999L5 19'
            stroke='#2D3436'
            strokeWidth='2'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
    case IconSizeProperties.small:
      return (
        <svg
          width='16'
          height='16'
          viewBox='0 0 16 16'
          fill='none'
          xmlns='http://www.w3.org/2000/svg'
        >
          <path
            d='M12.6668 12.6667L3.3335 3.33334'
            stroke='#2D3436'
            strokeWidth='1.6'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
          <path
            d='M12.6665 3.33333L3.33317 12.6667'
            stroke='#2D3436'
            strokeWidth='1.6'
            strokeLinecap='round'
            strokeLinejoin='round'
          />
        </svg>
      );
  }
}
