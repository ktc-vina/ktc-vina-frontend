import React from 'react';
import { IconSizeProperties } from '@/common/enum/icon-size';

export default function MinusSign(props: { size: IconSizeProperties }) {
  return (
    <svg
      width='24'
      height='24'
      viewBox='0 0 24 24'
      fill='none'
      xmlns='http://www.w3.org/2000/svg'
    >
      <path
        d='M7 12H17'
        stroke='#636E72'
        strokeWidth='2'
        strokeLinecap='round'
        strokeLinejoin='round'
      />
    </svg>
  );
}
