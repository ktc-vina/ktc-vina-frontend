import { ImageMetadata } from "@/lib/types/image-meta";
import Image from "next/image";

export default function AppImage(props: {
  containerWidth: number,
  containerHeight: number,
  image: ImageMetadata
}) {
  return (
    <Image
      src={props.image.url}
      width={props.image.width}
      height={props.image.height}
      alt={props.image.alt ?? 'image-text'}
      style={{
        objectFit: "contain",
        width: props.containerWidth,
        height: props.containerHeight
      }}
    />
  )
}