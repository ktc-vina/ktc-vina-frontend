import { BreadcrumbItem, BreadcrumbLink } from "@/components/ui/breadcrumb";
import Image from "next/image";

export default function HomeBreadcrumbItem() {
  return (
    <BreadcrumbItem className="flex items-center gap-2">
      <Image src="/home-logo.svg" alt="home-logo" width={22} height={22} />
      <BreadcrumbLink href="/">Home</BreadcrumbLink>
    </BreadcrumbItem>
  )
}