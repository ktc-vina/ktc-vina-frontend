import AppBanner from './(landing-page)/banner/app-banner';
import FirstSponsorSection from './(landing-page)/first-sponsor-section/first-sponsor-section';
import AppFooter from './(landing-page)/footer/app-footer';
import AppNavbar from './(landing-page)/navbar/app-navbar';
import SecondBanner from './(landing-page)/second-banner/second-banner';
import AllPostsSection from './(landing-page)/services/post/all-posts-section';
import AllServicesSection from './(landing-page)/services/product/all-services-section';
import MainContentLayout from './(common)/main-content-layout';
import { getLandingPageMetadata } from '@/lib/api/page-static-metadata-loader';
import { GenericServiceMetadata } from '@/lib/types/generic-service-metadata';

export interface LandingPageMetadata {
  bookService: GenericServiceMetadata;
  productService: GenericServiceMetadata;
  maintainanceService: GenericServiceMetadata;
  support: GenericServiceMetadata;
  news: GenericServiceMetadata;
}


export default async function Home() {
  const metadata = await getLandingPageMetadata();

  return (
      <main>
        {/* <FirstNotification /> */}
        <AppNavbar />
        <MainContentLayout>
          <AppBanner />
          <div className='mx-[5.5rem]'>
            <FirstSponsorSection />
            <AllServicesSection 
              bookService={metadata.bookService}
              productService={metadata.productService}
              maintainanceService={metadata.maintainanceService}
            />
            <SecondBanner />
            <AllPostsSection
              support={metadata.support}
              news={metadata.news}
            />
          </div>
          <footer>
            <AppFooter />
          </footer>
        </MainContentLayout>
      </main>
  );
}