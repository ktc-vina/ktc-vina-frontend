import { MetadataRoute } from "next";

export default function robots(): MetadataRoute.Robots {
  return {
    rules: {
      userAgent: "*",
      allow: "/",
      disallow: "/dashboard/",
    },
    sitemap:
      (process.env.NEXT_PUBLIC_API_BASE_URL ||
        "https://laptoptot.vn/") + "/sitemap.xml",
  };
}
