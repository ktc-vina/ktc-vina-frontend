export default function Copyright() {
  return (
    <div className="bg-primary text-white py-2 px-2 flex justify-between text-[14px] font-extralight">
      <p>Copyright© 2024 www.ktcvinabearing.comcom. All Rights Reserved.</p>
      <a href="iframe.vn" className="text-white underline hover:cursor-pointer">Design by iframe.vn - Website service</a>
    </div>
  )
}