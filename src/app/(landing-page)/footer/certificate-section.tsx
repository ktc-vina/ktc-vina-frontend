import Image from "next/image";

export default function CertificateSection() {
  return (
    <div className="mt-12 p-3 bg-neutral-2 flex items-center">
      {/* Title */}
      <div className="mr-[200px]">
        <h1 className="text-primary">Certificates</h1>
        <p className="body-text text-neutral-9">Chứng nhận chính hãng</p>
      </div>

      {/* Certificate image */}
      <Image
        src='/certificate.svg'
        alt="certificate"
        width={133}
        height={64}
      ></Image>
    </div>
  )
}