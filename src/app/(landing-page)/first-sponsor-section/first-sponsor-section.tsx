import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious } from "@/components/ui/carousel";
import Image from "next/image";

export default function Firstsponsorsection() {
  return (
    <div>
      <h1 className="tracking-tight">
        Chúng tôi tự hào là <span className="text-primary">Nhà phân phối Ủy Quyền </span> của: 
      </h1>
      <div className="mt-4 mb-10 py-2 bg-white rounded-lg">
        <Carousel>
          <CarouselContent className="items-center">
            <CarouselPrevious></CarouselPrevious>
            <CarouselItem className="basis-auto">
              <Image src='/sponsors/first.svg' alt='first-logo' width={0} height={0} style={{ width: 'auto', height: 'auto' }} className="mx-auto" />
            </CarouselItem>
            <CarouselItem className="basis-auto">
              <Image src='/sponsors/second.svg' alt='first-logo' width={0} height={0} style={{ width: 'auto', height: 'auto' }} className="mx-auto" />
            </CarouselItem>
            <CarouselItem className="basis-auto">
              <Image src='/sponsors/third.svg' alt='first-logo' width={0} height={0} style={{ width: 'auto', height: 'auto' }} className="mx-auto"/>
            </CarouselItem>
            <CarouselItem className="basis-auto">
              <Image src='/sponsors/fourth.svg' alt='first-logo' width={0} height={0} style={{ width: 'auto', height: 'auto' }} className="mx-auto"/>
            </CarouselItem>
            <CarouselItem className="basis-auto">
              <Image src='/sponsors/fifth.svg' alt='first-logo' width={0} height={0} style={{ width: 'auto', height: 'auto' }} className="mx-auto"/>
            </CarouselItem>
            <CarouselNext></CarouselNext>
          </CarouselContent>
        </Carousel>
      </div>
    </div>
  );
}
