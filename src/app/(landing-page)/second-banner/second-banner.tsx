export default function SecondBanner() {
  return (
    <div className="my-10 w-[1216px] h-[400px] bg-[url('/banner-image-ui-2.svg')] bg-cover flex items-end justify-center rounded-3xl">
      <p className="text-white text-[32px] py-6 font-medium">
        <span className="!text-primary">KTC Vina</span> tự hào được nhiều khách hàng doanh nghiệp tin cậy
      </p>
    </div>
  )
}
