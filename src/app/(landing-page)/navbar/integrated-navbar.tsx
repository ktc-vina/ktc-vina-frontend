"use client"
 
import * as React from "react"
 
import { cn } from "@/common/utils/utils"
import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu"
import Link from "next/link";

export interface NavigateMetadata {
  title: string;
  href: string;
  description?: string;
  subNavigate?: NavigateMetadata[]
}
 
const components: NavigateMetadata[] = [
  {
    title: 'Dịch vụ',
    href: '/services',
    subNavigate: [
      {
        title: 'Nhập khẩu trực tiếp và cung cấp sỉ & lẻ',
        href: '/services/nhap-khau-truc-tiep-va-cung-cap-si-le'
      },
      {
        title: 'Đo kiểm rung động',
        href: '/services/do-kiem-rung-dong'
      },
      {
        title: 'Cân chỉnh đồng tâm trục',
        href: '/services/can-chinh-dong-tam-truc'
      },
      {
        title: 'Căng chỉnh độ căng và độ phẳng dây Curoa',
        href: 'serivces/cang-chinh-do-cang-va-do-phang-day-curoa'
      },
      {
        title: 'Các dịch vụ cơ khí khác',
        href: '/services/cac-dich-vu-co-khi-khac'
      },
    ]
  },
  {
    title: 'Sản phẩm',
    href: '/products',
    subNavigate: [
      {
        title: 'Gối đỡ vòng bi',
        href: '/products/goi-do-vong-bi'
      },
      {
        title: 'Vòng bi FAG',
        href: '/products/vong-bi-fag'
      },
      {
        title: 'Vòng bi INA',
        href: '/products/vong-bi-ina'
      },
      {
        title: 'Vòng bi NMB',
        href: '/products/vong-bi-nmb'
      },
      {
        title: 'Vòng bi NRB',
        href: '/products/vong-bi-nrb'
      },
      {
        title: 'Các loại vòng bi khác',
        href: '/products/cac-loai-vong-bi-khac'
      },
      {
        title: 'Dầu mỡ bôi trơn Bechem',
        href: '/products/dau-mo-boi-tron-bechem'
      },
      {
        title: 'Load cell & Stepping motors',
        href: '/products/local-cell-and-stepping-motors'
      },
      {
        title: 'Máy kéo nén',
        href: '/products/may-keo-nen'
      },
      {
        title: 'Các sản phẩm công nghệ mới của Schaeffler',
        href: '/products/san-pham-cong-nghe-moi'
      },
    ]
  },
  {
    title: 'Công cụ bảo dưỡng',
    href: '#',
    subNavigate: [
      {
        title: 'Bộ gia nhiệt',
        href: '#'
      },
      {
        title: 'Cảo thủy lực',
        href: '#'
      },
      {
        title: 'Các giải pháp bôi trơn',
        href: '#'
      }
    ]
  },
  {
    title: 'Hỗ trợ kĩ thuật',
    href: '#'
  }
]
 
export function IntegratedNavBar() {
  return (
    <NavigationMenu>
      <NavigationMenuList>
        {components.map(navigateData => (
          <NavigationMenuItem key={navigateData.title}>
            {navigateData.subNavigate ? (
              <>
                <NavigationMenuTrigger className="bg-inherit hover:text-primar button-text" navLink={navigateData.href}>{navigateData.title}</NavigationMenuTrigger>
                <NavigationMenuContent>
                  <ul className="gap-3 overflow-hidden w-[500px] rounded-md bg-white">
                    {navigateData.subNavigate!.map((subNavigateData, index) => (
                      <ListItem
                        className={`${index === navigateData.subNavigate!.length - 1 ? '!border-b-[0px]' : ''}`}
                        href={subNavigateData.href}
                        title={subNavigateData.title}
                        key={subNavigateData.title}
                      >
                        {subNavigateData.description}
                      </ListItem>
                    ))}
                  </ul>
                </NavigationMenuContent>
              </>
            ) : (
              <Link href={navigateData.href} legacyBehavior passHref>
                <NavigationMenuLink className={navigationMenuTriggerStyle()}>
                  {navigateData.title}
                </NavigationMenuLink>
              </Link>
            )}
          </NavigationMenuItem>
        ))}
      </NavigationMenuList>
    </NavigationMenu>
  )
}
 
const ListItem = React.forwardRef<
  React.ElementRef<"a">,
  React.ComponentPropsWithoutRef<"a">
>(({ className, title, children, ...props }, ref) => {
  return (
    <li>
      <NavigationMenuLink asChild>
        <a
          ref={ref}
          className={cn(
            "block select-none py-3 border-b-[1px] border-neutral-2 px-4 leading-none no-underline outline-none transition-colors hover:bg-neutral-2",
            className
          )}
          {...props}
        >
          <div className="button-text-s">{title}</div>
          <p className="line-clamp-2 text-sm leading-snug text-muted-foreground">
            {children}
          </p>
        </a>
      </NavigationMenuLink>
    </li>
  )
})

ListItem.displayName = 'ListItem';