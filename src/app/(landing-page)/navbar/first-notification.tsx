import Image from "next/image"

export default function FirstNotification() {
  return (
    <div className="bg-primary text-white py-2 px-3 flex justify-between text-[14px] font-extralight rounded-b-md">
      <div className="flex items-center gap-1">
        <Image src="/white-location.svg" alt="location-white" width={19} height={20}/>
        <p>Văn phòng: 82/24 Đường Hoàng Bật Đạt, Phường 15, Quận Tân Bình, TP. Hồ Chí Minh.</p>
      </div>
      <div className="flex items-center gap-1">
        <Image src="/phone-white.svg" alt="phone-white" width={25} height={24}/>
        <p>0938 172 369 (Mr Hien)</p>
      </div>
    </div>
  )
}