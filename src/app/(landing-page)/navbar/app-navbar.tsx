import Image from 'next/image';
import { IntegratedNavBar } from './integrated-navbar';
import RightSection from './right-section';

export default async function AppNavbar(props: {
  className?: string
}) {
  return (
    // z-1000 is currently using for nav
    <nav className="sticky top-0 bg-gradient-to-b from-black/10 to-neutral-2 pt-[10px] z-[1000]">
      <div className={`flex items-center px-3 py-1 !text-neutral-7 bg-white/90 backdrop-blur rounded-xl w-[1400px] mx-auto ${props.className}`}>
        <div className='w-[40%] flex justify-start'>
          <IntegratedNavBar />
        </div>
        <a className='w-[20%] flex justify-center hover:cursor-pointer' href='/'>
          <Image src='/ktc-nav-logo.svg' alt='logo-header' width={177} height={56} style={{width: "auto", height: "auto"}} />
        </a>
        <div className='w-[40%] flex justify-end'>
          <RightSection />
        </div>
      </div>
    </nav>
  );
}
