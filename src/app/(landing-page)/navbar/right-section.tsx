'use client';

import CartIcon from '@/components/shared/icons/cart';
import SearchIcon from '@/components/shared/icons/search';
import { navigationMenuTriggerStyle } from '@/components/ui/navigation-menu';
import {
  NavigationMenu,
  NavigationMenuLink,
  NavigationMenuList,
} from '@radix-ui/react-navigation-menu';
import Link from 'next/link';

export default function RightSection() {
  return (
    <div className='flex items-center gap-2'>
      <NavigationMenu>
        <NavigationMenuList className='flex items-center'>
          <Link href='#' legacyBehavior passHref>
            <NavigationMenuLink className={navigationMenuTriggerStyle()}>
              Về chúng tôi
            </NavigationMenuLink>
          </Link>
          <Link href='#' legacyBehavior passHref>
            <NavigationMenuLink className={navigationMenuTriggerStyle()}>
              Tin tức
            </NavigationMenuLink>
          </Link>
          <NavigationMenuLink className={navigationMenuTriggerStyle()}>
            <SearchIcon />
          </NavigationMenuLink>
          <NavigationMenuLink className={navigationMenuTriggerStyle()}>
            <CartIcon />
          </NavigationMenuLink>
        </NavigationMenuList>
      </NavigationMenu>
      <div>
        <p className='nav-text'>
          <span className='text-primary font-semibold text-[16px]'>0938 172 369 &nbsp; </span>
          <span className='font-semibold text-[16px]'>(Mr Hien)</span>
        </p>
      </div>
    </div>
  );
}
