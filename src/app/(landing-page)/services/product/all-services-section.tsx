import ProductCarouselBox from "@/components/carousel/product-carousel/product-carousel-box"
import { GenericServiceMetadata } from "@/lib/types/generic-service-metadata"

export default function AllServicesSection(
  props: {
    bookService: GenericServiceMetadata,
    productService: GenericServiceMetadata,
    maintainanceService: GenericServiceMetadata
  }
){
  return <div>
  
  {/* Book services */}
  <ProductCarouselBox
    title = { props.bookService.title }
    items = { props.bookService.items }
    shortDescription = { props.bookService.shortDescription }
  ></ProductCarouselBox>

  {/* Product services */}
  <ProductCarouselBox
    title = { props.productService.title }
    items = { props.productService.items }
    shortDescription = { props.productService.shortDescription }
  ></ProductCarouselBox>

  {/* Maintainance service */}
  <ProductCarouselBox
    title = { props.maintainanceService.title }
    items = { props.maintainanceService.items }
    shortDescription = { props.maintainanceService.shortDescription }
  ></ProductCarouselBox>

  </div>
}