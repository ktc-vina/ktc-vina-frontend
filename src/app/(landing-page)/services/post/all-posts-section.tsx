import { GenericServiceMetadata } from "@/lib/types/generic-service-metadata";
import PostCarouselBox from "../../../../components/carousel/post-carousel/post-carousel-box";

export default function AllPostsSection(props: {
  support: GenericServiceMetadata,
  news: GenericServiceMetadata
}) {
  return <div>
  
  {/* support */}
  <PostCarouselBox
    title = { props.support.title }
    items = { props.support.items }
    shortDescription = { props.support.shortDescription }
  ></PostCarouselBox>

  {/* news */}
  <PostCarouselBox
    title = { props.news.title }
    items = { props.news.items }
    shortDescription = { props.news.shortDescription }
  ></PostCarouselBox>

  </div>
}