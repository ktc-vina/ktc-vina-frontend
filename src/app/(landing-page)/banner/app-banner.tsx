import { Link } from "lucide-react";
import Image from "next/image";

export default async function AppBanner() {
  return (
    <div className="mt-[56px]">
      {/* First section */}
      <div className="flex justify-start text-[47px] tracking-[-1px] text-primary">
        CÔNG TY TNHH KTC VINA
      </div>

      {/* Second section */}
      <div className="flex justify-end">
        <div className="w-50% text-[36px] font-light text-neutral-7">
          <p className="text-right leading-tight">
            Chuyên phân phối <span className="!text-primary">vòng bi công nghiệp</span>
            <br />
            phân khúc ủy quyền chính hãng tại Việt Nam
          </p>
        </div>
      </div>

      {/* Third section */}
      <div className="flex justify-end gap-3 pb-[30px] pt-[12px]">
        <Image src="arrow-right.svg" width={69} height={14} alt="arrow-right"/>
        <a href="/products" className="px-[24px] rounded-3xl text-primary border-primary border-[1px] text-[28px]">
          Xem sản phẩm
        </a>
      </div>

      {/* Banner image */}
      <div className='w-[100%] flex justify-center my-6'>
        <Image src='/banner-image-ui.svg' alt='logo-header' width={1400} height={392} style={{ width: '100%', height: 'auto' }}/>
      </div>
    </div>
  );
}
