import { IconSizeProperties } from '@/common/enum/icon-size';
import ImageContainer from '@/components/image-container';
import ArrowBottom from '@/components/shared/icons/arrow-bottom';
import React from 'react';

export default function TestPage() {
  return (
    <>
      <div>TestPage</div>
      <h1>Test h1</h1>
      <ArrowBottom size={IconSizeProperties.small} />
      <ArrowBottom size={IconSizeProperties.large} />
      <ArrowBottom size={IconSizeProperties.medium} />
      <p className='w-[300px] nav-text'>
        CÔNG TY TNHH KTC VINA được thành lập và chính thức đi vào hoạt động từ
        tháng 8 năm 2011. Với những dòng sản phẩm chất lượng ổn định, định hướng
        phát triển đúng đắn, KTC VINA  đã trở thành một thương hiệu mạnh trong
        lĩnh vực phân phối Vòng bi công nghiệp phân khúc ủy quyền chính hảng tại
        Việt Nam.
      </p>
      <ImageContainer
        containerWidth={1400}
        containerHeight={600}
        image={{
          url: '/service-dummy.svg',
          width: 280,
          height: 182,
          alt: 'Service dummy'
        }}
      />
    </>
  );
}
