import PostCarouselBox from "@/components/carousel/post-carousel/post-carousel-box"
import { GenericServiceItem } from "@/lib/types/generic-service-item"

export default function ProductsByTypeNews(props: {
  title: string,
  news: GenericServiceItem[],
}) {
  return (
    <>
      {/* news */}
      <PostCarouselBox
        className="pt-[24px] pb-[8px] mt-[10px]"
        title = { props.title }
        items = { props.news }
        shortDescription = { '' }
      ></PostCarouselBox>
    </>
  )
}