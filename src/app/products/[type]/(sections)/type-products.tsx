import ProductCarouselDoubleLineItems from "@/components/carousel/product-carousel/product-carousel-double-line-items"
import { GenericServiceItem } from "@/lib/types/generic-service-item"

export default function TypeProducts(props: {
  title: string,
  products: GenericServiceItem[][]
}) {
  return (
    <div className="bg-white p-[24px] rounded-xl mt-[10px]">
      <h4 className="text-primary">Sản phẩm - {props.title}</h4>
      <ProductCarouselDoubleLineItems items={props.products} />
    </div>
  )
}