import ProductBreadcrumb from "@/components/breadcrumb/products-breadcrumb";
import MainContentLayout from "@/app/(common)/main-content-layout";
import AppFooter from "@/app/(landing-page)/footer/app-footer";
import AppNavbar from "@/app/(landing-page)/navbar/app-navbar";
import { BreadcrumbRouteMetadata } from "@/lib/types/breadcrumb-route-metadata";
import ProductDetailInformation from "./(sections)/product-detail-information";
import ProductDetailTabsView from "./(sections)/product-detail-tabs-view";
import MoreProductSection from "./(sections)/more-products-section";

const metadata = {
  name: 'Gối đỡ UCP210-J7 FAG',
  images: [
    {
      url: '/image-product-detail.svg',
      width: 492,
      height: 368
    }
  ],
  isAvailable: true,
  productType: 'Hàng liên hệ',
  technicalInformation: {
    type: 'Gối đỡ vòng bi',
    brand: 'Schaeffler',
    code: 'UCP210-J7',
    from: 'Vietnam'
  },
  detailDescription: `Gối đỡ vòng bi UCP210-J7 Bộ gối đỡ UCP bao gồm: Vòng bi cầu chèn hướng tâm và gối đỡ là các bộ phận máy mạnh mẽ, sẵn sàng lắp ráp cho thiết kế kiểu bố trí vòng bi mang tính tiết kiệm. Chúng chủ yếu bao gồm một vòng bi cầu chèn hướng tâm lắp trong gối đỡ. Lỗ của gối đỡ và vòng ngoài của vòng bi có biên dạng hình cầu và ăn khớp với nhau, nhằm điều chỉnh vòng của vòng bi trong gối đỡ cho các sai lệch trục tĩnh. Nhờ kiểu gắn đặc biệt chủ yếu là vành khóa lệch tâm hoặc vít cấy, vòng bi cầu chèn hướng tâm dễ dàng gắn lên trên trục. Chúng đặc biệt dễ lắp khi được sử dụng kết hợp với trục chịu kéo. Vòng bi cầu chèn hướng tâm được trang bị phớt có nhiều chi tiết, bảo vệ hiệu quả bộ con lăn không bị nhiễm bẩn và độ ẩm, ngay cả trong điều kiện vận hành khó khăn và khắc nghiệt. Schaeffler cung cấp đa dạng các cấu kiện gối đỡ với vòng bi cầu chèn hướng tâm. Vòng bi cầu chèn hướng tâm có thiết kế dựa trên vòng bi cầu rãnh sâu một dãy.`,
  similarProducts: [
    {
      name: 'Vòng bi FAG',
      routerLink: '#6',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#7',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#8',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#9',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#10',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    }
  ],
  extraProducts: [
    {
      name: 'Vòng bi FAG',
      routerLink: '#6',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#7',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#8',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#9',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#10',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    }
  ]
};

export default function ProductDetail({ params }: { params: { type: string, id: string } }) {
  const typeRouteMetadata: BreadcrumbRouteMetadata = {
    title: params.type,
    routerLink: `/products/${params.type}`
  };
  const detailRouteMetadata: BreadcrumbRouteMetadata = {
    title: params.id,
    routerLink: '#'
  }
  return (
    <>
      <AppNavbar/>
      <MainContentLayout>
        {/* Breadcrumb */}
        <div className="mt-0.5 py-1 bg-white rounded-md">
          <ProductBreadcrumb type={typeRouteMetadata} detail={detailRouteMetadata} />
        </div>

        {/* Main section */}
        <main className="w-[1204px] mx-auto mt-[10px]">
          <h3 className="mb-[10px] py-2 px-4 text-neutral-8 bg-white rounded-lg">
            {metadata.name}
          </h3>

          <ProductDetailInformation
            isAvailable = { metadata.isAvailable }
            fullDescription = { metadata.detailDescription }
            technicalInfo = { metadata.technicalInformation }
            images = { metadata.images }
            type = { metadata.productType }
          />
          <ProductDetailTabsView
            detailDescription = { metadata.detailDescription }
            technicalInfo = { metadata.technicalInformation }
          />
          <MoreProductSection 
            sameProducts = { metadata.similarProducts }
            moreProducts = { metadata.extraProducts }
            typeRouterLink = {`/products/${params.type}`}
          />
        </main>

        <AppFooter />
      </MainContentLayout>
    </>
  )
}