import ProductCarouselBox from "@/components/carousel/product-carousel/product-carousel-box"
import { GenericServiceItem } from "@/lib/types/generic-service-item"

const productServiceMetadata = {
  title: 'Sản phẩm cùng loại',
  shortDescription: '',
  items: [
    {
      name: 'Vòng bi FAG',
      routerLink: '#6',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#7',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#8',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#9',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#10',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    }
  ]
}

const productServiceMetadata2 = {
  title: 'Có thể bạn quan tâm',
  shortDescription: '',
  items: [
    {
      name: 'Vòng bi FAG',
      routerLink: '#6',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#7',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#8',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#9',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Vòng bi FAG',
      routerLink: '#10',
      image: {
        url: '/service-dummy-2.svg',
        width: 280,
        height: 182
      },
      hoverImage: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    }
  ]
}

export default function MoreProductSection(props: {
  typeRouterLink: string,
  sameProducts: GenericServiceItem[],
  moreProducts: GenericServiceItem[]
}) {
  return (
    <div className="mt-[10px]">
      {/* San pham cung loai */}
      <ProductCarouselBox
        title = "Sản phẩm cùng loại"
        items = { props.sameProducts }
        shortDescription = ""
        routerLink = { props.typeRouterLink }
      ></ProductCarouselBox>

      {/* Co the ban quan tam */}
      <ProductCarouselBox
        title = "Có thể bạn quan tâm"
        items = { props.moreProducts }
        shortDescription = ""
        routerLink = "/products"
      ></ProductCarouselBox>
    </div>
  )
}