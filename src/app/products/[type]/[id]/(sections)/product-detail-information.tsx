import { ImageMetadata } from "@/lib/types/image-meta";
import ProductTechnicalInfo from "@/lib/types/product-technical-info";
import Image from "next/image";
import ProductGallery from "./product-gallery";

const dummyDescription = `Gối đỡ vòng bi UCP210-J7 Bộ gối đỡ UCP bao gồm: Vòng bi cầu chèn hướng tâm và gối đỡ là các bộ phận máy mạnh mẽ, sẵn sàng lắp ráp cho thiết kế kiểu bố trí vòng bi mang tính tiết kiệm. Chúng chủ yếu bao gồm một vòng bi cầu chèn hướng tâm lắp trong gối đỡ. Lỗ của gối đỡ và vòng ngoài của vòng bi có biên dạng hình cầu và ăn khớp với nhau, nhằm điều chỉnh vòng của vòng bi trong gối đỡ cho các sai lệch trục tĩnh. Nhờ kiểu gắn đặc biệt chủ yếu là vành khóa lệch tâm hoặc vít cấy, vòng bi cầu chèn hướng tâm dễ dàng gắn lên trên trục. `;

export default function ProductDetailInformation(props: {
  images: ImageMetadata[],
  isAvailable: boolean,
  type: string,
  technicalInfo: ProductTechnicalInfo,
  fullDescription: string
}) {
  return (
    <div className="w-full flex gap-2">

      {/* Left column with 6 cols */}
      <div className="w-1/2 bg-white rounded-xl p-[16px]">
        <div className="relative w-full h-full">
          {/* TODO: replace with carousel image */}
          <ProductGallery images={props.images} />
        </div>
      </div>

      {/* Right column with 6 cols */}
      <div className="w-1/2 px-[20px] pt-[16px] pb-[20px] bg-white rounded-xl">

        {/* product status */}
        <div className="p-[8px] flex items-center gap-6 border-b-[2px]">
          {props.isAvailable === true ? (
            <div className="flex items-center gap-2">
              <Image 
                src="/tick.svg"
                width={24}
                height={24}
                alt="tick-icon"
              />
              <p className="body-text-2 text-neutral-7">Còn hàng</p>
            </div>
          ) : (<></>)}
          <div className="flex items-center gap-2">
            <Image 
              src="/call-yellow.svg"
              width={24}
              height={24}
              alt="call-icon"
            />
            <p className="body-text-2 text-neutral-7">{props.type}</p>
          </div>
        </div>

        {/* product properties */}
        <div className="w-full p-[16px] border-b-[2px] flex flex-col gap-2 body-text-2">
          {/* Danh muc */}
          <p>
            <span className="font-bold text-neutral-8">Danh mục:&nbsp;</span>
            <span className="text-neutral-7">{props.technicalInfo.type}</span>
          </p>
          {/* Thuong hieu */}
          <p>
            <span className="font-bold text-neutral-8">Thương hiệu:&nbsp;</span>
            <span className="text-neutral-7">{props.technicalInfo.brand}</span>
          </p>
          {/* Ma san pham */}
          <p>
            <span className="font-bold text-neutral-8">Mã sản phẩm:&nbsp;</span>
            <span className="text-neutral-7">{props.technicalInfo.code}</span>
          </p>
          {/* Xuat Xu */}
          <p>
            <span className="font-bold text-neutral-8">Xuất xứ:&nbsp;</span>
            <span className="text-neutral-7">{props.technicalInfo.from}</span>
          </p>
        </div>

        {/* Description */}
        <div className="w-full py-[16px] px-[8px] text-neutral-7 border-b-[2px]">
          <p className="line-clamp-5">
            {props.fullDescription}
          </p>
        </div>

        {/* button section */}
        <div className="w-full py-[16px] px-[8px] border-b-[2px]">
          <button className="bg-primary w-full rounded-xl text-white py-3 button-text">
            LIÊN HỆ CHÚNG TÔI
          </button>
        </div>

        {/* contact section */}
        <div className="px-[8px] pt-[16px]">
          <div className="flex items-center gap-4">
            <Image src="/verified-icon.svg" alt="verified" width={16} height={16} />
            <p className="body-text-2 text-accent-1">Liên hệ đặt hàng : 0938 172 369 (Mr Hien) </p>
          </div>
          <div className="flex items-center gap-4">
            <Image src="/published-icon.svg" alt="verified" width={16} height={16} />
            <p className="body-text-2 text-accent-1">khanhhienktc@gmail.com</p>
          </div>
        </div>
      </div>
    </div>
  )
}