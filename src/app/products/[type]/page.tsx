import ProductBreadcrumb from "@/components/breadcrumb/products-breadcrumb";
import AppNavbar from "@/app/(landing-page)/navbar/app-navbar";
import AppFooter from "@/app/(landing-page)/footer/app-footer";
import GenericTopSection from "@/app/(common)/generic-top-section";
import TypeProducts from "./(sections)/type-products";
import ProductsByTypeNews from "./(sections)/type-news";
import { BreadcrumbRouteMetadata } from "@/lib/types/breadcrumb-route-metadata";
import ProductTypeInformation from "./(sections)/product-type-information";
import MainContentLayout from "@/app/(common)/main-content-layout";
import { GenericServiceItem } from "@/lib/types/generic-service-item";
import { getProductsByTypeMetadata } from "@/lib/api/page-static-metadata-loader";

export interface ProductTypeDetailMetadata {
  id: string;
  type: string;
  description: string;
  products: GenericServiceItem[][]; // 2 row
  news: GenericServiceItem[];
}

export default async function ProductsByType({ params }: { params: { type: string } }) {
  const typeRouteMetadata: BreadcrumbRouteMetadata = {
    title: params.type,
    routerLink: '#'
  }

  const metadata = await getProductsByTypeMetadata();

  return (
    <>
      {/* Navbar and breadcrumbs */}
      <AppNavbar />
      <MainContentLayout>
        {/* Breadcrumb */}
        <div className="mt-0.5 py-1 bg-white rounded-md">
          <ProductBreadcrumb type={typeRouteMetadata} />
        </div>

        {/* Main section */}
        <main className="w-[1204px] mx-auto mt-[10px]">
          <GenericTopSection
            sectionHeader="Gối đỡ vòng bi"
            image={
              {
                url: "/generic-product-image.svg",
                width: 1204,
                height: 240
              }
            }  
          />
          <ProductTypeInformation type={metadata.type} description={metadata.description} />
          <TypeProducts title={metadata.type} products={metadata.products} />
          <ProductsByTypeNews title={metadata.type} news={metadata.news} />
        </main>

        <AppFooter />
      </MainContentLayout>
    </>
  )
}