import AppNavbar from "../(landing-page)/navbar/app-navbar";
import ProductBreadcrumb from "../../components/breadcrumb/products-breadcrumb";
import GenericTopSection from "../(common)/generic-top-section";
import AllProductsSection from "./all-products-section";
import AppFooter from "../(landing-page)/footer/app-footer";
import MainContentLayout from "../(common)/main-content-layout";

export default function Products() {
  return (
    <>
      <AppNavbar />
      <MainContentLayout>
        <div className="mt-0.5 py-1 bg-white rounded-md">
          <ProductBreadcrumb />
        </div>

        {/* Main section */}
        <main className="w-[1204px] mx-auto mt-[10px]">
          <GenericTopSection
            sectionHeader="Tất cả sản phẩm"
            image={
              {
                url: "/generic-product-image.svg",
                width: 1204,
                height: 240
              }
            }  
          />
          <AllProductsSection />
        </main>

        <AppFooter />
      </MainContentLayout>
    </>
  )
}