import { GenericServiceItem } from "@/lib/types/generic-service-item";
import ProductCarouselBox from "../../components/carousel/product-carousel/product-carousel-box"
import { getAllProductsPageMetadata } from "@/lib/api/page-static-metadata-loader";

export interface ProductTypeMetadata {
  title: string;
  routerLink: string;
  shortDescription: string;
  items: GenericServiceItem[];
}

export default async function AllProductsSection(){
  const serviceMetadata = await getAllProductsPageMetadata();

  return <div className="mt-[10px]">
  {serviceMetadata.map(metadata => (
    <ProductCarouselBox
      title = { metadata.title }
      items = { metadata.items }
      routerLink = { metadata.routerLink }
      shortDescription = { metadata.shortDescription }
      key = { metadata.title }
    ></ProductCarouselBox>
  ))}
  </div>
}