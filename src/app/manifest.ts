import { MetadataRoute } from 'next'
 
export default function manifest(): MetadataRoute.Manifest {
  return {
    "name": "laptoptot.vn",
    "short_name": "laptoptot.vn",
    "description": "Laptoptot.vn - Chuyên các dòng laptop xách tay từ USA: workstation, ultrabook, gaming, đồ họa, văn phòng. Sản phẩm chất lượng, giá tốt. Uy tín, bảo hành đầy đủ. 0946 093 093 - 74/6 Vườn Lài, P. Tân Thành, Q. Tân Phú, TP.HCM.",
    "icons": [],
    "scope": "/"
  }
}

/*
"icons": [
      {
        "src": "/SEO/mobile-icon/android-icon-36x36.png",
        "sizes": "36x36",
        "type": "image/png",
      },
      {
        "src": "/SEO/mobile-icon/android-icon-48x48.png",
        "sizes": "48x48",
        "type": "image/png",
      },
      {
        "src": "/SEO/mobile-icon/android-icon-72x72.png",
        "sizes": "72x72",
        "type": "image/png",
      },
      {
        "src": "/SEO/mobile-icon/android-icon-96x96.png",
        "sizes": "96x96",
        "type": "image/png",
      },
      {
        "src": "/SEO/mobile-icon/android-icon-144x144.png",
        "sizes": "144x144",
        "type": "image/png",
      },
      {
        "src": "/SEO/mobile-icon/android-icon-192x192.png",
        "sizes": "192x192",
        "type": "image/png",
      },
    ],
    "theme_color": "#005669",
    "background_color": "#FFF",
    "start_url": "/",
    "display": "standalone",
    "orientation": "portrait",
    "related_applications": [
      {
        "platform": "play",
        "url": "https://play.google.com/store/apps/details?id=vn.tiki.app.tikiandroid",
        "id": "vn.tiki.app.tikiandroid"
      },
      {
        "platform": "itunes",
        "url": "https://apps.apple.com/vn/app/tiki-shopping-fast-shipping/id958100553"
      },
      {
        "platform": "webapp",
        "url": "https://tiki.vn/manifest.json"
      }
    ],
 */