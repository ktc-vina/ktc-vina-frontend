import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import '@/styles/globals.css';
import '@/styles/default.css';
import { SWRProvider } from '@/components/layout/swr-provider';
import NextTopLoader from 'nextjs-toploader';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: 'Đại lý Vòng bi - Dầu mỡ bôi trơn - KTC VINA',
  description: 'Đại lý Vòng bi - Dầu mỡ bôi trơn - KTC VINA',
};

export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <html lang='en'>
      <body className={`${inter.className} bg-neutral-2 w-full`}>
        <main>
          <SWRProvider>
            <NextTopLoader color="#EC2227"/>
            {children}
          </SWRProvider>
        </main>
      </body>
      {/* <Script
          id='laptoptot-ld-json-store'
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonDataStore) }}
          strategy='lazyOnload'
      />
      <Script
          id='laptoptot-ld-json-service'
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonDataService) }}
          strategy='lazyOnload'
      />
      <Script
          id='laptoptot-ld-json-website'
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonDataWeb) }}
          strategy='lazyOnload'
      />
      <Script src="https://www.googletagmanager.com/gtag/js?id=G-2DWWDV2HL5" />
      <Script id="google-analytics">
        {`
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());
 
          gtag('config', 'G-2DWWDV2HL5');
        `}
      </Script> */}
    </html>
  );
}
