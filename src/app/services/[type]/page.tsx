import ServicesBreadcrumb from "@/components/breadcrumb/services-breadcrumb";
import AppFooter from "@/app/(landing-page)/footer/app-footer";
import AppNavbar from "@/app/(landing-page)/navbar/app-navbar";
import { BreadcrumbRouteMetadata } from "@/lib/types/breadcrumb-route-metadata";
import ServiceTypeInformation from "./service-type-information";
import ProductCarouselBox from "@/components/carousel/product-carousel/product-carousel-box";
import MainContentLayout from "@/app/(common)/main-content-layout";

const description = 'Nhà phân phối Uỷ Quyền vòng bi Công Nghiệp INA, FAG – CHLB ĐỨC\nNhà phân phối Uỷ Quyền Vòng bi NMB- Japan (Mine Bearing), Các loại vòng bi nhỏ, siêu nhỏ dùng trong các ngành máy công cụ, Dệt may, Nha khoa, Quạt điện …..\n\nNhà phân phối các loại vòng OEM các loại vòng bi kim đến từ India, vòng bi kim chuyên dụng.\n\nNgoài ra chúng tôi còn cấp vòng bi NSK- ASAHI – IKO,EZO – Nhật Bản, TIMKEN - TORRINGTON – Mỹ. Đặt biệt các loại vòng bi chuyên dụng như: Vòng cao tốc chính xác, vòng bi 1 chiều,  vòng bi kim, bi trượt, vòng bi inox….\n\nDụng cụ tháo lắp vòng bi, dầu mỡ chuyên dùng cho vòng bi, Dụng cụ đồ nghề sử dụng cho bảo trì sủa chữa cơ khí, các loại cảo chuyên dụng.\n\nVới lực lượng kỹ thuật nhiều năm kinh nghiệm, Chúng tôi sẽ chọn ra những loại bạc đạn, dây đai, phốt … phù hợp với từng vị trí sử dụng của quý công ty nhằm đem lại hiệu quả kinh tế - kỹ thuật cao nhất cho các chi tiết truyền động.\n\n'

const bookServiceMetadata = {
  title: 'Một số dịch vụ khác',
  items: [
    {
      name: 'Đo kiểm rung động',
      routerLink: '#2',
      image: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Cân chỉnh đồng tâm trục',
      routerLink: '#3',
      image: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Căng chỉnh độ căng và độ phẳng dây curoa',
      routerLink: '#4',
      image: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    },
    {
      name: 'Các dịch vụ cơ khí khác',
      routerLink: '#5',
      image: {
        url: '/service-dummy.svg',
        width: 280,
        height: 182
      }
    }
  ]
}

export default function ServicesByType({ params }: { params: { type: string } }) {
  const typeRouteMetadata: BreadcrumbRouteMetadata = {
    title: params.type,
    routerLink: '#'
  }

  return (
    <>
      {/* Navbar and breadcrumbs */}
      <AppNavbar />

      <MainContentLayout>
        <div className="mt-0.5 py-1 bg-white rounded-md">
          <ServicesBreadcrumb type={typeRouteMetadata} />
        </div>

        {/* Main section */}
        <main className="w-[1204px] mx-auto mb-[48px]">
          <h3 className="mt-[10px] mb-[10px] py-2 px-4 text-neutral-8 bg-white rounded-lg">
            {'Nhập khẩu trực tiếp và cung cấp sỉ & lẻ'}
          </h3>
          <ServiceTypeInformation
            description={description}
            image={{url: '/type-demo-image.svg', width: 100, height: 100}}
          />

          {/* Book services */}
          <ProductCarouselBox
            className="mt-[32px]"
            title = { bookServiceMetadata.title }
            items = { bookServiceMetadata.items }
            shortDescription = { `` }
          ></ProductCarouselBox>
        </main>

        <AppFooter />
      </MainContentLayout>
    </>
  )
}