import ServicesByTypeBox from "./services-by-type-box"
import { ImageMetadata } from "@/lib/types/image-meta";
import { getAllServicesPageMetadata } from "@/lib/api/page-static-metadata-loader";

export interface ServiceTypeMetadata {
  id: string,
  routerLink: string,
  header: string;
  description: string;
  image: ImageMetadata
}

export default async function AllServicesSection(){
  const serviceMetadata = await getAllServicesPageMetadata();

  return <div>
  {serviceMetadata.map(metadata => (
    <ServicesByTypeBox
      header = { metadata.header }
      description = { metadata.description }
      image = { metadata.image }
      routerLink = { metadata.routerLink }
      key = { metadata.header }
    ></ServicesByTypeBox>
  ))}
  </div>
}