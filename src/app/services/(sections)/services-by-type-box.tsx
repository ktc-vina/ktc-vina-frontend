import { Button } from "@/components/ui/button";
import { ImageMetadata } from "@/lib/types/image-meta";
import Image from "next/image";
import Link from "next/link";

export default function ServicesByTypeBox(props: {
  header: string,
  description: string,
  image: ImageMetadata,
  routerLink: string
}) {
  return (
    <div className="p-3 rounded-xl flex items-center gap-6 bg-white mt-[10px]">
      <Image src={props.image.url} alt="service" width={props.image.width} height={props.image.height} />

      {/* Content */}
      <div className="pr-[100px]">
        <p className="text-primary font-medium text-[23px]">{props.header}</p>
        <p className="text-neutral-7 text-[14px] leading-6 py-3">{props.description}</p>
        <Link href={props.routerLink}>
          <Button variant="outline" className="text-neutral-8 hover:text-primary hover:border-primary">Xem chi tiết</Button>
        </Link>
      </div>
    </div>
  )
}