import AppNavbar from "../(landing-page)/navbar/app-navbar";
import GenericTopSection from "../(common)/generic-top-section";
import AppFooter from "../(landing-page)/footer/app-footer";
import AllServicesSection from "./(sections)/all-services-section";
import ServicesBreadcrumb from "../../components/breadcrumb/services-breadcrumb";
import MainContentLayout from "../(common)/main-content-layout";

export default function Services() {
  return (
    <>
      <AppNavbar />
      <MainContentLayout>
        <div className="mt-0.5 py-1 bg-white rounded-md">
          <ServicesBreadcrumb />
        </div>

        {/* Main section */}
        <main className="w-[1204px] mx-auto mt-[10px]">
          <GenericTopSection
            sectionHeader="Tất cả dịch vụ"
            image={
              {
                url: "/generic-product-image.svg",
                width: 1204,
                height: 240
              }
            }  
          />
          <AllServicesSection />
        </main>
        
        <AppFooter />
      </MainContentLayout>

    </>
  )
}