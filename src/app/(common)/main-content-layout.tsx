export default function MainContentLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {
  return (
    <main className="w-[1400px] mx-auto">
      {children}
    </main>
  )
}